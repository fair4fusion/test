#Get Access Token

First of all, use a **browser** to access http://rest-operator.fair4fusion.iit.demokritos.gr/get_token .
Sign in using **gitlab-f4f** option.

After that, you will be redirected to a page that contains a json. Find access_token key and copy its value (without " ").

Open a terminal and type `access_token=<access_token_value>` , where <access_token_value> is the string you copied before.

#API endpoints

- Get status of pods that have been instantiated by f4f operator: ` curl "http://rest-operator.fair4fusion.iit.demokritos.gr/status/" -H "Authorization: Bearer $access_token" `
- Get status of all pods in cluster:  ` curl "http://rest-operator.fair4fusion.iit.demokritos.gr/status_all/" -H "Authorization: Bearer $access_token" `
- Show results: ` curl "http://rest-operator.fair4fusion.iit.demokritos.gr/show?arg=<path>" -H "Authorization: Bearer $access_token"  `
- Fetch results: ` curl "http://rest-operator.fair4fusion.iit.demokritos.gr/fetch?arg=<path>/<file>" -H "Authorization: Bearer $access_token"  `
- Deploy an experiment in cluster ` curl http://rest-operator.fair4fusion.iit.demokritos.gr/deploy -X POST -F myFile=@<fileName> -H "Authorization: Bearer $access_token" `
- Add query shot : ` curl "http://rest-operator.fair4fusion.iit.demokritos.gr/addQuery" -X POST -F fileName=@<fileName> -H "Authorization: Bearer $access_token" `
