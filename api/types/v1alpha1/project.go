package v1alpha1

//go:generate controller-gen object paths=$GOFILE

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

type Workflow struct {
	Image     string `json:"image" yaml:"image"`
	Name      string `json:"name" yaml:"name"`
	WorkLabel string `json:"workLabel,omitempty" yaml:"workLabel,omitempty"`
}

type F4FSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Work           []Workflow `json:"work" yaml:"work"`
	ComparisonType string     `json:"comparisonType,omitempty" yaml:"comparisonType,omitempty"`
	// Label is the value of the 'daemon=' label to set on a node that should run the daemon

	// Mapping phase is set to true if we currently are in mapping phase.When we proceed to reduce phase, it becomes false
	Phase        int32  `json:"Phase" yaml:"Phase"`
	ExperimentId string `json:"experimentId,omitempty" yaml:"experimentId,omitempty"`
}

// F4FStatus defines the observed state of F4F
type F4FStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	// Count is the number of nodes the image is deployed to
	Count int32 `json:"count" yaml:"count"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// F4F is the Schema for the f4fs API
type F4F struct {
	metav1.TypeMeta   `json:",inline" yaml:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`

	Spec   F4FSpec   `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status F4FStatus `json:"status,omitempty" yaml:"status,omitempty"`
}

// +kubebuilder:object:root=true

// F4FList contains a list of F4F
type F4FList struct {
	metav1.TypeMeta `json:",inline" yaml:",inline"`
	metav1.ListMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Items           []F4F `json:"items" yaml:"items"`
}
