module rest.f4f.com

go 1.13

require (
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/pquerna/cachecontrol v0.1.0 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
	gopkg.in/yaml.v2 v2.2.4
	k8s.io/apimachinery v0.17.2
	k8s.io/client-go v0.17.2
)
